# Copyright: Tevin Coles 2023

def compute_area_rect(l,l2):
    area = l * l2
    return area
def compute_area_circle(length):
    area = (length **2) * 3.14
    return area

shape = ""

while shape != "quit":

    shape = input("What shape do you have? Square, Circle, Rectangle. Type quit to exit.")
    
    if shape.lower() == 'rectangle':
        len = float(input("Input the length: "))
        len2 = float(input("Input the width: "))
        print(f"The area of a rectangle with sides length {len} and {len2} is {compute_area_rect(len, len2)} units squared.\n")
    
    elif shape.lower() == 'circle':
        len = float(input("Input the radius: "))
        print(f"The area of a circle with radius length {len} is {compute_area_circle(len)} units squared.\n")
    
    elif shape.lower() == 'square':
        len = float(input("Input the length: "))
        len2 = len
        print(f"The area of a square with sides length {len} is {compute_area_rect(len, len2)} units squared.\n")

    elif shape != "quit":
        print('You are stupid pick a shape in the list.')
  