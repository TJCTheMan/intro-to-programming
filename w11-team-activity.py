# Copyright: Tevin Coles 2023

with open("hr_system.txt") as file:
    for line in file:
        line_no_space = line.strip()
        parts = line_no_space.split(" ")

        name = parts[0].strip()
        id_num = parts[1].strip()
        title = parts[2].strip()
        salary = float(parts[3])
        paycheck = salary/24
        if title == "Engineer":
            paycheck += 1000
    

        print(f"{name} (ID: {id_num}), {title} - ${(paycheck):,.2f}")