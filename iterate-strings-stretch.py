# Copyright: Tevin Coles 2023

string = "In coming days, it will not be possible to survive spiritually without the guiding, directing, comforting, and constant influence of the Holy Ghost."

run_again = "yes"
while run_again == "yes" :
    choice = int(input("Input a number n and the program will capitalize every n-th letter in the string. "))

    for i, letter in enumerate(string):
        
        if i != 0:
            if i % choice == 0:
                print(letter.upper(), end="")
            else:
                print(letter.lower(), end="")
        else:
            print(letter.lower(), end="")

    print()

    run_again = input("Would you like to enter another number? ")

print("BYE!!!")

