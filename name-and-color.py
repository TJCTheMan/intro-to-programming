# Copyright: Tevin Coles 2023
import time

#Asks the user for their name and favorite color
name = input("What is your name? ")
favColor = input("And what is your favorite color? ")
time.sleep(.5)
#Prints a hello message
print("Hi there, " + name + "!")

#The user gets told that the program's favorite color is blue
if (favColor != "Blue" and favColor != "blue"): 
    print("Your favorite color is " + favColor + "! That's cool! My favorite color is Blue!")


#If the favorite color of the user is blue, they get a special message.
elif (favColor == "Blue" or "blue"):
    print("Your favorite color is " + favColor +"!? No way, that is my favorite color as well!")