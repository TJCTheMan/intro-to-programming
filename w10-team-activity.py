# Copyright: Tevin Coles 2023
import math

account_input = None
balance_input = None
accounts = []
balances = []


print(f'Enter the names and balances of bank accounts (type quit when done)\n')


while account_input != "quit":
    account_input = input("What is the name of this account? ")
    if account_input != "quit":
        balance_input = float(input(f"What is the balance of {account_input}? "))
        accounts.append(account_input)
        balances.append(balance_input)
    

else:
    print(f"Account Information: ")
    for i in range(len(accounts)):
        print(f"{accounts[i]} - ${balances[i]:.2f}")

    

    print(f"\nTotal: ${(math.fsum(balances)):.2f}.")

    print(f"Average: ${(math.fsum(balances)) / len(accounts):.2f}.")

    max_num = balances[0]
    for i in balances:
        if i > max_num:
            max_num = i

    print(f"The account with the highest balance is {accounts[balances.index(max_num)]} with a total of ${balances[balances.index(max_num)]:.2f}")
