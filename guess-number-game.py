# Copyright: Tevin Coles 2023

import random, math



playagain = "yes"

while playagain.lower() == "yes":
    user_random_num = random.randint(1,1_000_000_000_000_000)
    user_guesses = 0
    user_guess = int(input("What is your guess? "))
    user_guesses += 1

    while user_guess != user_random_num:

        if user_guess > user_random_num:
            print(f"Lower")
            user_guess = int(input("What is your guess? "))
            user_guesses += 1

            continue

        else: 
            print(f"Higher")
            user_guess = int(input("What is your guess? "))
            user_guesses += 1

            continue

    else:
        print(f"You guessed correctly!")
        print(f"It took you {user_guesses} guesses! ")
        
        playagain = input("Do you want to play again? ")
        continue

