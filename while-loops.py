# Copyright: Tevin Coles 2023

number = int(input("Please type a positive number: "))

while (number < 0 ):
    print(f"Sorry, {number} is a negative number. Please try again.")
    number = int(input("Please type a positive number: "))

print(f"The number is: {number}")




can_have_candy = ""

while (can_have_candy.lower() != 'yes'):
    can_have_candy = input("May I Have a piece of candy? ")

print(f"Thank you. ")