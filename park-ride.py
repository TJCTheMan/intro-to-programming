# Copyright: Tevin Coles 2023

# Take some user input:
height_1 = int(input("What is the height of the first rider? Round to nearest inch and input the number.: "))

age_1 = int(input("What is the age of the first rider? Input the number.: "))

# Ask if there is a second rider
second_rider = input("Is there a second rider? (yes/no): ")

if (second_rider.lower() == 'yes'):

    # Get some input about the second rider
    height_2 = int(input("What is the height of the second rider?: "))
    
    age_2 = int(input("What is the age of the second rider?: "))
    
    # Two Riders? Go here.
    if (age_1 < 18 or age_2 < 18 and height_1 >= 36 and height_2 >= 36):
    
        if (age_1 >= 12 and age_2 >= 12 and height_1 >= 52 and height_2 >= 52):
    
            can_ride = True
    
        elif (age_1 < 18 and age_1 > 11):
            
            # Dealing with passports part 1
            golden_passport_1 = input("Does rider 1 have a golden passport? (True/False)")
            
            if (golden_passport_1.lower() == 'true' or age_2 >= 18):
                
                can_ride = True
    
            else:
                golden_passport_2 = input("Does rider 2 have a golden passport? (True/False)")
    
                if (golden_passport_2.lower() == 'true' or age_1 >= 18):
           
                    can_ride = True
                
                else:
                    can_ride = False

        elif (age_2 < 18 and age_2 > 11):

            # Dealing with passports part 2    
            golden_passport_2 = input("Does rider 2 have a golden passport? (True/False)")
    
            if (golden_passport_2.lower() == 'true' or age_1 >= 18):
           
                can_ride = True
           
            else:
                golden_passport_1 = input("Does rider 1 have a golden passport? (True/False)")
            
                if (golden_passport_1.lower() == 'true' or age_2 >= 18):
                
                    can_ride = True
    
                else:
    
                    can_ride = False
           
               
    else:
        
        # Stretch Challenge 3
        if (age_1 >= 16 and age_2 >= 14) or (age_2 >= 16 and age_1 >= 14):
        
            can_ride = True
        
        else:
        
            can_ride = False
            
    
elif second_rider == 'no':
    
    # Single rider stuff.
    if height_1 > 35:
    
        if height_1 >= 62 and age_1 >= 18:
    
            can_ride = True
    
        elif age_1 < 18 and age_1 > 11:
    
            golden_passport = input("Do you have a golden passport? (True/False)")
    
            if golden_passport.lower() == 'true':
    
                if height_1 >= 52:
    
                    can_ride = True
    
                else:
    
                    can_ride = False
    
            else:
    
                can_ride = False
    
        else:
    
             can_ride = False
    else:
    
        can_ride = False

else:
    print(f"That was not the correct input please try again.\n")
    can_ride = False




if can_ride == True:
    print(f"Welcome to the ride. Please be safe and have fun!\n")
else:
    print(f"Sorry, you may not ride.\n")