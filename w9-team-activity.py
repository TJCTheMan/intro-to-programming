# Copyright: Tevin Coles 2023
import math, time 

list_of_numbers = []
smallest = None
# user_input = None
print("Enter a list of numbers, enter 0 or enter nothing to end.")
time.sleep(0.5)
user_input = int(input("Enter Number: "))

# Take inputs from user until they input 0.
while (user_input != 0):
    list_of_numbers.append(user_input)
    user_input = int(input("Enter Number: "))

# Take the list of numbers and do stuff with it.

else:
    print(f'The sum is: {math.fsum(list_of_numbers):.0f}')
    print(f'The average is: {(math.fsum(list_of_numbers)) / (len(list_of_numbers))}')
    print(f'The largest number is: {max(list_of_numbers)}')
# find the smallest positive number
    for num in list_of_numbers:
        if (smallest is None and num > 0):
            smallest = num
        elif (0 < num < smallest):
            smallest = num

    print(f"The smallest positive number is: {smallest}")

    list_sorted = sorted(list_of_numbers)

    print("The sorted list is: ")

    for i in list_sorted:
        print(i)


    