# Copyright: Tevin Coles 2023

# Ask the user for some integers
print("Please input two integers (whole, positive numbers). ")
num1 = int(input("The first number: "))
num2 = int(input("The second number: "))

# Compare said integers and tell the user which is bigger.
if (num1>num2):
    print(f"Your first number is larger than your second number.")

else:
    print(f"Your first number is not larger than your second number.")

if (num1 == num2):
    print(f"Your two numbers are equal.")

else:
    print(f"Your two numbers are not equal.")

if (num1<num2):
    print(f"Your second number is larger than your first number.")

else:
    print(f"Your second number is not larger than your first number.")


fav_animal = input("What is your favorite animal? ")
my_fav_animal = "badger"

if (fav_animal.lower() == my_fav_animal):
    print(f"That is also my favorite animal!")

else:
    print(f"That is not my favorite animal, my favorite animal is a {my_fav_animal.lower()}")


