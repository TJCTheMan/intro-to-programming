# Copyright: Tevin Coles 2023


temp_input = float(input('What temperature would you like to know the windchill for? '))

f_or_c = input('Farenhiet or Celcius (F/C)? ')

def c_to_f(temp):
    temp_input_f = round(((temp * (9/5)) + 32), 2)
    return temp_input_f
def f_to_c(temp):
    temp_input_c = round(((temp * (5/9)) - 32), 2)
    return temp_input_c

def windchill(temp_input):

    chill = round((35.74 + (0.6215*temp_input) - 35.75*(wind ** 0.16) + 0.4275*temp_input*(wind ** 0.16)), 2)
    return chill


if f_or_c.lower() == 'c':
    temp_input_f = c_to_f(temp_input)



for i in range(1,13):
    wind = i * 5
    if f_or_c.lower() == "f":
        print(f"At temp {temp_input}{f_or_c.upper()}, and wind speed {i*5} mph, the wind chill is {windchill(temp_input)}{f_or_c.upper()}")

    if f_or_c.lower() == "c":
        windchill_temp = windchill(temp_input_f)

        windchill_c = f_to_c(windchill_temp)
        
        print(f"At temp {temp_input}{f_or_c.upper()}, and wind speed {i*5} mph, the wind chill is {windchill_c}{f_or_c.upper()}")