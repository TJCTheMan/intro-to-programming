# Copyright: Tevin Coles 2023
import math
import time

sq_side = input("Input the length of one side of a square. (Please DO NOT input units, use only numbers.) ")
print ("We will use centimeters for this program.")
area_square = (float(sq_side) ** 2)
print (f"The area of the square is: {float(area_square)} cm^2, or {(float(area_square))/10000} m^2.")


rect_len = input("Input the length of a rectangle: ")
rect_width = input("Input the width of a rectangle: ")
area_rect = (float(rect_len) * float(rect_width))
print (f"The area of the rectangle is {float(area_rect)} cm^2, or {(float(area_rect))/10000} m^2.")



circle_radius = input("Input the radius of a circle: ")

area_circle = ((float(circle_radius)  ** 2) * float(math.pi))
print (f"The area of the circle is {round(float(area_circle),3)} cm^2, or {round(((float(area_circle))/10000), 3)} m^2")



print (f"For the next part of the program, I only need one measurement.")
num_universal = float(input ("Input a number: "))
area_square_2 = round(float(num_universal ** 2), 2)
area_circle_2 = round(float(math.pi * num_universal ** 2), 3)
vol_cube = round(float(num_universal ** 3), 2)
vol_sphere = round(float((num_universal * math.pi * (4/3)) **3), 3)

print (f"The area of a square with that measurement as the length of a side is: {area_square_2} cm^2.")
print (f"The area of a circle with that measurement as the radius is: {area_circle_2} cm^2.")
print (f"The volume of a cube with that measurement as the length of a side is: {vol_cube} cm^3.")
print (f"The volume of a sphere with that measurement as the length of a side is: {vol_sphere} cm^3.")


