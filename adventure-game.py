# Copyright: Tevin Coles 2023
print(f"Let's play a game! You are walking down a trail. You come to a fork in the trail.")

x=1

while x == 1:

    print(f" The trail forks to the left and the right. Which path do you take?")
    user_input_1 = input("'LEFT', 'RIGHT', 'TURN AROUND': ")

    if user_input_1.lower() == 'left':
        print(f"If you choose to go left at the fork in the trail, it will lead you deeper into the forest. The trees become denser here, blocking out much of the sunlight and casting dappled shadows on the ground. As you continue along the winding path, you notice that the underbrush becomes thicker and more tangled, making it harder to see what lies ahead. Do you continue to forge ahead? Or do you turn back?")
        user_input_1_1 = input("'KEEP GOING', 'TURN BACK': ")
        if user_input_1_1.lower() == "turn back":
            continue
        
        elif user_input_1_1.lower() == "keep going":
            print(f"As you forge onward, you come across a small clearing with a babbling brook running through its center. The sound of rushing water is soothing. Do you stop and rest? Do you drink from the brook? Do you press onward? Or turn back?")
            user_input_1_1_1 = input("'REST', 'DRINK', 'KEEP GOING', 'TURN BACK': ")

            if user_input_1_1_1.lower() == "rest":
                print(f"As you rest by the brook, you feel yourself slowly slip into a dreamless sleep. However you never wake up.... The End.")
                x = 2

            elif user_input_1_1_1.lower() == "drink":
                print(f"As you take a sip from the babbling brook, the cold water clears your mind. You realize that the only way home is back the way you came. However...")
                continue

            elif user_input_1_1_1.lower() == "keep going":
                print(f"As you continue onward, you slip and fall into a ravine. Your death is painful, but quick. The End.")
                x = 2
                
            elif user_input_1_1_1.lower() == "turn back":
                continue
                
            else:
                print(f"That wasn't one of the options. Please Restart The Game.")
                x = 2
            
            
    elif user_input_1.lower() == 'right':
        print(f"As you progress along the right fork, the trail twists and turns, eventually getting you hoplessly lost. You can see a hill in the distance and you seem to be going that direction. Do you climb a tree for a better vantage point? Do you continue on towards the hill? Or do you try to find your way back?")
        user_input_1_2 = input("'CLIMB TREE', 'GO TOWARDS HILL', 'TURN BACK': ")
        if user_input_1_2.lower() == "turn back":
            print(f"Just when you think all hope is lost, and you're never going to make it out of this forest,....")
            break
        
        elif user_input_1_2.lower() == "go towards hill":
            print(f"As you get close to the hill, the sunlight peeks out above the clouds! As you near the crest, the view opens before you, and you realize that you weren't that lost to begin with. You can clearly see the path home from here. Everything will be all right. Congratulations, you made it! ")
            x = 2
        
        elif user_input_1_2.lower() == "climb tree":
            print(f"As you get close to the top of the tree you realize that you weren't that lost to begin with. You can clearly see the path home from here. Everything will be all right. Congratulations, you made it! ")
            x = 2
        else:
            print(f"That wasn't one of the options. Restart The Game.")
            x = 2
        
    elif user_input_1.lower() == 'turn around':
        print(f"You turn around, but somehow the trail keep leading you back to this fork...")
        continue
    else:
        print("That wasn't one of the options. Please try again.")
        continue


