# Copyright: Tevin Coles 2023
import time
import math

#gets the user's age, converts it to an INT, and adds 5 to it.
age = input("How old are you? ")
age_plus_five = (int(age) +5) 

print (f"In five years, you will be {age_plus_five} years old.")


# Asks the user how many egg cartons they have, and mathematically deduces the number of eggs.
num_cartons = input("How many egg cartons do you have? ")
num_eggs = (int(num_cartons) * 12) 

print (f"With that many egg cartons, you can store {num_eggs} eggs.")


#cookies per person. Asks the user for how many cookies they have and divides it by the number of people. 
#will divide evenly, and print the remainder.

num_cookies = input("How many cookies do you have? ")
num_people = input("How many people are eating said cookies? ")
cookies_per_person = math.floor((int(num_cookies))/(int(num_people)))
cookies_left_over = ((int(num_cookies))%(int(num_people)))

print (f"With that many cookies, {num_people} people get {cookies_per_person} cookies each, and you will have {cookies_left_over} cookie(s) left over.")
