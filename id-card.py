# Copyright: Tevin Coles 2023

import time
#Ask the User for information
print('Please enter the following information:')
first_name = input("First name: ")
last_name = (input("Last name: "))
address = input("Address: ")
email = input("Email Address: ")
phone_number = input("Phone Number (formatted (xxx) xxx-xxxx): ")
job_title= input("Job Title: ")
id_number = input("ID Number: ")
hair_color = input("Hair Color: ")
eye_color = input("Eye Color: ")
month_started = input('The month you started: ')
training_completed = input('Did you complete training? Yes or No: ')

time.sleep(2)

#print the data from the user in a readable ID CARD format
print()
print()
print("--------------------------------------------")
print(f"{last_name.upper()}, {first_name.capitalize()}")
print(job_title.title())
print(f"ID: {id_number}")
print()
print(email)
print(phone_number)
print()
print(f"{'Hair: ' + hair_color:<25} Eyes: {eye_color}")
print(f"{'Month: ' + month_started:<25} Training: {training_completed}")
print("--------------------------------------------")

