# Copyright: Tevin Coles 2023

import random

play_again = "yes"

# Loop to manage play again
while play_again.lower() == "yes":

    secret_word_list = ["bacon", "soup", "spaghetti", "pizza", "sandwich"]
    secret_word = random.choice(secret_word_list)

    hint = ["_" for _ in secret_word]

    num_guesses = 0
    print(f"Welcome to the word guessing game! \nI have a secret word, and it is your job to guess it. ")
    print (f"Your hint is {' '.join(hint)} . ")

    # Loop to manage the actual gameplay
    while True:
        hint = ["_" for _ in secret_word]  #Resets hint so random stuff doesn't stick around.
        try:
            user_guess = input("What is your guess? ")
            num_guesses += 1

        except IndexError: #This is  only here for the above try so it works.
            break
        
        # This is for the hint updating.
        for i, letter in enumerate(user_guess):
            try:
                if letter == secret_word[i]:
                    hint[i] = letter.upper()
                elif letter in secret_word:
                    hint[i] = letter.lower()
            except IndexError:
                break

        # handling wrong lengths
        if len(user_guess) != len(secret_word):
            print(f"Sorry, the guess must have the same number of letters as the secret word.")
            continue

        elif user_guess.lower() != secret_word.lower():
            print(f"That isn't correct. Guess again. \n Your hint is {' '.join(hint)} . ")
            continue

# Success condition
        else:
            print(f"That is correct! The secret word was {secret_word.lower()}! It took you {num_guesses} guesses! " )
            play_again = input("Would you like to play again? ")
            break    
            
else: 
    print(f"Thanks for playing! ")