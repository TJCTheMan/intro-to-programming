# Copyright: Tevin Coles 2023
import math

cart = []
prices = []

action = ""

print(f'''
Please select one of the following: 
1. Add item
2. View cart
3. Remove item
4. Compute total
5. Quit''')

while action != 5:
    
    action = int(input("Please enter an action: "))
    if action == 1:
        item = input("What item would you like to add? ")
        price = float(input(f"What is the price of {item}? "))
        cart.append(item)
        prices.append(price)
        continue
    elif action == 2:
        for i in range(len(cart)):
            print(f"{i + 1:2}. {cart[i]} -- ${prices[i]:.2f}")
        continue
    elif action == 3:
        item_rm = int(input('What item would you like to remove? '))
        if item_rm in (range(len(cart) + 1)):
            cart.pop(item_rm - 1)
            prices.pop(item_rm - 1)
            print(f"Item removed. ")
        else:
            print(f"That isn't a valid number. ")

    elif action == 4:
        print(f"The subtotal price of the items in the shopping cart is ${math.fsum(prices):.2f}.")

        print(f"The sales tax (in Idaho) will be ${(math.fsum(prices)) * 0.06:.2f}.")

        print(f"The total price is ${(math.fsum(prices)) * 1.06:.2f}.")

        


else:
    print(f"Thank you, Goodbye.")