# Copyright: Tevin Coles 2023
input_temp = float(input("Please input a temperature using only numbers: "))
temp_unit = input("Is this in Fahrenheit or Celsius? (F/C): ")

if (temp_unit == "C"):
    temp_out = ((input_temp + 32) * (9 / 5))
    print(f"Your temperature when converted to Fahrenheit is : {temp_out:.1f} F")

elif (temp_unit == "F"):
    temp_out = ((input_temp - 32) * (5 / 9))
    print(f"Your temperature when converted to Celsius is : {temp_out:.1f} C")

else:
    print('You did\'nt input a correct Temperature Unit. Shutting Down.')

