# Copyright: Tevin Coles 2023

string = "commitment"
choice = input("What is your favorite letter? ")

for letter in string:
    if letter == choice:
        print("_", end="")
    else:
        print(letter.lower(), end="")
        