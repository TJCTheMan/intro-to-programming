# Copyright: Tevin Coles 2023
import time
while True:
    print("Please enter the following to get a madlib!")

    adjective_1 = input("Adjective: ")
    noun_1 = input("Noun: ")
    adjective_2 = input("Adjective: ")
    proper_noun_1 = input("Proper Noun: ")
    verb_1 = input("Verb (present tense): ")
    verb_2 = input("Verb: ")
    noun_2 = input("Noun: ")

    print(f"Your {adjective_1.lower()} {noun_1.lower()} went to {proper_noun_1.capitalize()} to {verb_1.lower()} and {verb_2.lower()} my {noun_2.lower()}.")
    time.sleep (1)
    try_again =input("Would you like to try again? Press Y or N. ")
    if (try_again == "Y"):
        continue
    elif(try_again == "N"):
        break
    else:
        print(f"You didn't choose yes or no, so the program will end now.")
        break

else:
    print(f"The program is ending.")
