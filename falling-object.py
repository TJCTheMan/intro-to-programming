# Copyright: Tevin Coles 2023
import math

print(f"HI\nThis is a Velocity calculator.\nPlease enter the following values without units.")

mass = float(input("Mass (in kg): "))
gravity = float(input("Gravity (in m/s^2, 9.8 for Earth, 24 for Jupiter): "))
time = float(input("Time (in seconds): "))
density = float(input("Density of the fluid (in kg/m^3, 1.3 for air, 1000 for water): "))
area = float(input("Cross sectional area (in m^2): "))
drag = float(input("Drag constant (0.5 for sphere, 1.1 for cylinder): "))


#Math Time

inner_value = ((1/2) * density * area * drag)
velocity_at_time_t = float((math.sqrt((mass * gravity) / inner_value)) * (1 - math.exp(((-1 * (math.sqrt(mass * gravity * inner_value) / mass) * time)))))


terminal_velocity = float((math.sqrt((mass * gravity) / inner_value)))


print(f"The inner value of c is {round(inner_value, 3)}.")
print(f"The Velocity is {round(velocity_at_time_t, 3)} m/s.")
print(f"The Terminal Velocity is {round(terminal_velocity, 3)} m/s.")

