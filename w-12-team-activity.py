# Copyright: Tevin Coles 2023

most_chapters = -1
most_chapters_bom = -1
book_max_bom = ""
user_scripture = input("Which volume of Scripture would you like facts on? ")
with open("books_and_chapters.txt") as file:
    for line in file:
        parts = line.strip().split(":")
    
        book = parts[0]
        num_chapters = int(parts[1])
        scripture = parts[2]

        # print(f"Scripture: {scripture}, Book: {book}, Chapters: {num_chapters}\n")
        if num_chapters > most_chapters:
                most_chapters = num_chapters
                book_most = book
        

        if scripture.lower() == user_scripture.lower():
            print(f"Scripture: {scripture}, Book: {book}, Chapters: {num_chapters}\n")

            if num_chapters > most_chapters_bom:
                most_chapters_bom = num_chapters
                book_max_bom = book
        
    print(f"The Book with the most chapters is {book_most} with {most_chapters} chapters.")
    print(f"The {user_scripture} book with the most chapters is {book_max_bom} with {most_chapters_bom} chapters.")
    
