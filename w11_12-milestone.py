# Copyright: Tevin Coles 2023
import os
life_low = 100000000000000000000
life_high = 0
life_low_user = 100000000000000000000
life_high_user = 0
lines_from_year = []
year_total = 0

with open("./life-expectancy.csv") as file:
    file_list = file.read()
    lines_list = file_list.splitlines()

    for line_number, line in enumerate(lines_list):
        
        parts = line.strip().split(",")
        country = parts[0]
        year = float(parts[2])
        life = float(parts[3])

        if life > life_high:
            life_high = life
            line_num_life_high = line_number
            

        if life < life_low:
            life_low = life
            line_num_life_low = line_number
            
    user_year = int(input("Enter the year of interest: "))
    print("")
    
    for line_number, line in enumerate(lines_list):
        
        parts = line.strip().split(",")
        country = parts[0]
        year = float(parts[2])
        life = float(parts[3])

        if line_number == line_num_life_high:
            print(f"The overall max life expectancy is: {life:.2f} from {country.capitalize()} in {int(year)}")
            
        if line_number == line_num_life_low:
            print(f"The overall min life expectancy is: {life:.2f} from {country.capitalize()} in {int(year)}")

        if year == user_year:
            file2=open("temp.txt", "a")
            file2.write(line)
            file2.write("\n")
            file2.close

            


with open("./temp.txt") as file2:
    file_list = file2.read()
    lines_from_year = file_list.splitlines()         

    for line_number, line in enumerate(lines_from_year):
        
        parts = line.strip().split(",")
        country = parts[0]
        year = float(parts[2])
        life = float(parts[3])

        if life > life_high_user:
            life_high_user = life
            line_num_life_high_user = line_number
            

        if life < life_low_user:
            life_low_user = life
            line_num_life_low_user = line_number

        

        year_total += life


    year_avg = year_total / len(lines_from_year)

    print(f"\nFor the year {user_year}: ")

    print (f"The average life expectancy across all countries was {year_avg:.2f}")

    for line_number, line in enumerate(lines_from_year):
        
        parts = line.strip().split(",")
        country = parts[0]
        year = float(parts[2])
        life = float(parts[3])

        if line_number == line_num_life_high_user:
                print(f"The max life expectancy was in {country.capitalize()} with {life:.2f}")
                    
        if line_number == line_num_life_low_user:
            print(f"The min life expectancy was in {country.capitalize()} with {life:.2f}")
            
if os.path.exists("./user-years-selected.txt"):
    os.remove("user-years-selected.txt")

os.rename("./temp.txt", "user-years-selected.txt")

        
    






        