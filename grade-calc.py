
# Copyright: Tevin Coles 2023

# I import these in everything even if I don't need them because they get used so much.
import math, time
# INPUT
grade_percent = float(input("What percentage grade did you get? "))

# Letter Assignment
if grade_percent >= 90:
    letter_grade = "A"
elif grade_percent >= 80:
    letter_grade = "B"
elif grade_percent >= 70:
    letter_grade = "C"
elif grade_percent >= 60:
    letter_grade = "D"
else:
    letter_grade = "F"
    
# Sign +/-
if((grade_percent < 93) and (grade_percent > 60) and grade_percent%10 >= 7):
    sign = "+"
elif((grade_percent < 93) and (grade_percent > 60) and grade_percent % 10 < 3):
    sign = "-"
else:
    sign = ""

# OUTPUT
if (grade_percent > 90):
    print(f"You got an {letter_grade}{sign}! Good Job!")
elif (grade_percent > 70):
    print(f"You got a {letter_grade}{sign}. You passed, but there is still room to improve!")
else:
    print(f"You got a {letter_grade}{sign}. Try again, you can do better!")
