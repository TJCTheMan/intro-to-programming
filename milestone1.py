# Copyright: Tevin Coles 2023
import math
import time

#asks the user for the prices of the meals
print("For this program you only need to input numerical values. ")
price_child = float(input("What is the price of a kid's meal? "))
price_adult = float(input("What is the price of an adult's meal? "))

#asks the user for the number of meals that need to be paid for
num_child_meals = int(input("How many kid's meals need to be paid for? "))
num_adult_meals = int(input("How many adult's meals need to be paid for? "))

#asks the user for tip amount and sales tax percentage
tax_percent = float(input("What is the sales tax percentage in your state/country/region? "))
tip_percent = float(input("What percentage would you like to tip? "))



#math time
subtotal = round(float((num_child_meals * price_child)+(num_adult_meals * price_adult)), 2)
tax_amount = float(subtotal * (tax_percent/100))
tip = float((tip_percent/100) * (subtotal + tax_amount))
total_price = round((subtotal + tax_amount), 2)
print(f'''Subtotal: ${subtotal} 
Sales Tax: ${tax_amount}
Tip: ${round((tip),2)}
Total (not including tip): ${total_price}
Total with tip: ${round(float(total_price + tip), 2)}''')

while True:

    #payment is taken, change is given, if the user didn't pay enough, they get to try again.
    payment_amount = float(input("What is the payment amount? "))


    if(payment_amount > (total_price + tip)):
        change = round((payment_amount - (total_price + tip)), 2)
        print(f"Your change is ${change}.")
        break

    elif(payment_amount == round((total_price + tip), 2)):
        print(f"You paid with exact change! Good for you!")
        break

    elif(payment_amount < (total_price + tip)):
        print(f"That isn't enough to cover the bill and the tip, would you like to try again? ")
        payment_amount = 0
        continue

time.sleep (1)
print(f"Have a nice day! Thanks for coming!")